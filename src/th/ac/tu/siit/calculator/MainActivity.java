package th.ac.tu.siit.calculator;

import android.os.Bundle;
import android.R.integer;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity
	implements OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        Button b0 = (Button)findViewById(R.id.num0);
        b0.setOnClickListener(this);
        Button b1 = (Button)findViewById(R.id.num1);
        b1.setOnClickListener(this);
        Button b2 = (Button)findViewById(R.id.num2);
        b2.setOnClickListener(this);
        Button b3 = (Button)findViewById(R.id.num3);
        b3.setOnClickListener(this);
        Button b4 = (Button)findViewById(R.id.num4);
        b4.setOnClickListener(this);
        Button b5 = (Button)findViewById(R.id.num5);
        b5.setOnClickListener(this);
        Button b6 = (Button)findViewById(R.id.num6);
        b6.setOnClickListener(this);
        Button b7 = (Button)findViewById(R.id.num7);
        b7.setOnClickListener(this);
        Button b8 = (Button)findViewById(R.id.num8);
        b8.setOnClickListener(this);
        Button b9 = (Button)findViewById(R.id.num9);
        b9.setOnClickListener(this);
        
        ((Button)findViewById(R.id.add)).setOnClickListener(this);
        ((Button)findViewById(R.id.sub)).setOnClickListener(this);
        ((Button)findViewById(R.id.mul)).setOnClickListener(this);
        ((Button)findViewById(R.id.div)).setOnClickListener(this);
        
        ((Button)findViewById(R.id.ac)).setOnClickListener(this);
        ((Button)findViewById(R.id.bs)).setOnClickListener(this);
        
        ((Button)findViewById(R.id.dot)).setOnClickListener(this);
        ((Button)findViewById(R.id.equ)).setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    int state = 0;
    // state==0 initial
    // state==1 input one operand
    // state==2 output generated
    
    int lastButton = 0;
    // lastButton==0 numeric
    // lastButton==1 operator
    
    double value1, value2, result;
    int operator;
    
	@Override
	public void onClick(View v) {
		int id = v.getId();
		
		TextView outputTV = (TextView)findViewById(R.id.output);
		TextView operatorTV = (TextView)findViewById(R.id.operator);
		String output = outputTV.getText().toString();
		
		switch(id) {
			case R.id.num0:
			case R.id.num1:
			case R.id.num2:
			case R.id.num3:
			case R.id.num4:
			case R.id.num5:
			case R.id.num6:
			case R.id.num7:
			case R.id.num8:
			case R.id.num9:
				if (output.equals("0") || lastButton==1) {
					output = "";
				}
				output = output + ((Button)v).getText().toString();
				outputTV.setText(output);
				lastButton = 0;
				break;
			case R.id.ac:
				outputTV.setText("0");
				lastButton = 0;
				state = 0;
				break;
			case R.id.bs:
				int l = output.length();
				if (l > 1) {
					output = output.substring(0, output.length()-1);
				}
				else {
					output = "0";
				}
				outputTV.setText(output);
				lastButton = 0;
				break;
			case R.id.add:
			case R.id.sub:
			case R.id.mul:
			case R.id.div:
				if (state == 0) {
					value1 = Double.parseDouble(output);
					operator = id;
					state = 1;
					displayOperator(operatorTV, operator);
				}
				else if (state == 1) {
					value2 = Double.parseDouble(output);
					if (operator == R.id.add) {
						value1 = value1 + value2;
					}
					else if (operator == R.id.sub) {
						value1 = value1 - value2;
					}
					else if (operator == R.id.mul) {
						value1 = value1 * value2;
					}
					else if (operator == R.id.div) {
						value1 = value1 / value2;
					}
					outputTV.setText(String.valueOf(value1));
					operator = id;
					displayOperator(operatorTV, operator);
					state = 1;
				}
				else if (state == 2) {
					value1 = Double.parseDouble(output);
					operator = id;
					displayOperator(operatorTV, operator);
					state = 1;
				}
				lastButton = 1;
				break;
			case R.id.equ:
				if (state == 1) {
					value2 = Double.parseDouble(output);
					if (operator == R.id.add) {
						value1 = value1 + value2;
					}
					else if (operator == R.id.sub) {
						value1 = value1 - value2;
					}
					else if (operator == R.id.mul) {
						value1 = value1 * value2;
					}
					else if (operator == R.id.div) {
						value1 = value1 / value2;
					}
					outputTV.setText(String.valueOf(value1));
					operator = 0;
					displayOperator(operatorTV, 0);
					state = 2;
				}
				lastButton = 0;
				break;
			case R.id.dot:
				if (output.indexOf('.') == -1) {
					output = output + ".";
				}
				break;
		} 
	}
    
	void displayOperator(TextView t, int operator) {
		switch(operator) {
		case R.id.add:
			t.setText("ADD");
			break;
		case R.id.sub:
			t.setText("SUBTRACT");
			break;
		case R.id.mul:
			t.setText("MULTIPLY");
			break;
		case R.id.div:
			t.setText("DIVIDE");
			break;
		default:
			t.setText("");
			break;
		}
	}
}
